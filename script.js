// DOM elements
const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");
let intervalId;

// Set canvas dimensions to match the window size
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Store sparkles
const sparkles = [];

canvas.addEventListener("mousemove", (event) => {
    // Get mouse coordinates
    const corX = event.clientX;
    const corY = event.clientY;

    // Clear the canvas
    context.clearRect(0, 0, canvas.width, canvas.height);

    // Draw the three big circles
    context.beginPath();
    context.fillStyle = "blue";
    context.arc(corX, corY, 30, 0, 2 * Math.PI);
    context.fill();

    context.beginPath();
    context.fillStyle = "red";
    context.arc(corX, corY, 20, 0, 2 * Math.PI);
    context.fill();

    context.beginPath();
    context.fillStyle = "yellow";
    context.arc(corX, corY, 10, 0, 2 * Math.PI);
    context.fill();

    // Draw sparkles and remove them if they go out of bounds
    for (let i = sparkles.length - 1; i >= 0; i--) {
        const sparkle = sparkles[i];

        context.beginPath();
        context.fillStyle = sparkle.color;
        context.arc(sparkle.x, sparkle.y, sparkle.size, 0, 2 * Math.PI);
        context.fill();

        // Update sparkle position
        sparkle.x += sparkle.speedX;
        sparkle.y += sparkle.speedY;

        // Check if the sparkle is out of bounds
        if (
            sparkle.x < 0 ||
            sparkle.x > canvas.width ||
            sparkle.y < 0 ||
            sparkle.y > canvas.height
        ) {
            sparkles.splice(i, 1); // Remove the sparkle from the array
        }
    }

    // Add new sparkle
    addSparkle(corX, corY);
});

// Function to add a sparkle to the canvas
function addSparkle(x, y) {
    const sparkle = {
        x,
        y,
        size: Math.random() * 5 + 2, // Random size between 2 and 7
        color: getRandomColor(),
        speedX: (Math.random() - 0.5) * 2, // Random horizontal speed
        speedY: (Math.random() - 0.5) * 2, // Random vertical speed
    };
    sparkles.push(sparkle);

    // Limit the number of sparkles to prevent performance issues
    if (sparkles.length > 100) {
        sparkles.shift(); // Remove the oldest sparkle
    }
}

// Function to change the background color with a random color
function changeBackgroundColor() {
    canvas.style.backgroundColor = getRandomColor();
}

// Start changing the background color at a specified interval (slower)
intervalId = setInterval(changeBackgroundColor, 2000); // Change color every 2000 milliseconds (2 seconds)

// Function to generate a random color
function getRandomColor() {
    const letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

// Update canvas dimensions when the window is resized
window.addEventListener("resize", () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
});
